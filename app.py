from flask import Flask, render_template, request, redirect, send_file, jsonify
from elasticsearch_dsl import connections, Search
import os

app = Flask(__name__)

connections.create_connection(hosts=[os.getenv('ES_HOST', 'localhost')], timeout=20)


@app.route('/test')
def test_page():
    return render_template('index.html')


# TODO: Remove URL Placeholder
def make_text_response(response, amount):
    return jsonify({
        "type": "text",
        "texts": [{"text": res.text, "url": 'https://www.google.com/'} for res in response[:amount]]
    })


@app.route('/search', methods=['POST'])
def index():
    term = request.form.get('term')
    amount = int(request.form.get('amount'))
    print(term)

    s = Search(index="en_wiki") \
        .query("match", title=term) \
        .exclude("match", description="beta")
    response = s.execute()
    if len(response) > 0:
        return make_text_response(response, amount)

    s = Search(index="en_wiki") \
        .query("match_phrase", title=term) \
        .exclude("match", description="beta")
    response = s.execute()
    if len(response) > 0:
        return make_text_response(response, amount)

    s = Search(index="en_wiki") \
        .query("match", text=term) \
        .exclude("match", description="beta")
    response = s.execute()
    if len(response) > 0:
        return make_text_response(response, amount)

    s = Search(index="en_wiki") \
        .query("match_phrase", text=term) \
        .exclude("match", description="beta")
    response = s.execute()
    if len(response) > 0:
        return make_text_response(response, amount)

    return jsonify({
        "type": "miss"
    })


if __name__ == '__main__':
    app.debug = True
    app.run(host="0.0.0.0", port=5000)
