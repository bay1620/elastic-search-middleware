FROM python:3.8

RUN pip install elasticsearch-dsl==6.4.0 Flask==1.1.2
RUN mkdir app
COPY . app
WORKDIR app

#RUN pip install -r requirements.txt

EXPOSE 5000

ENV FLASK_APP=app.py
ENV SERVER_NAME=0.0.0.0

ENTRYPOINT flask run -h 0.0.0.0