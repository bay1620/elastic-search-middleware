# Elastic Search Middleware

This provides an interface between Elasticsearch and the standard request format as defined by the [Related Items project](https://gitlab.rrz.uni-hamburg.de/bay1620/wilps-related-items/).

Get up and running quickly with the [docker image](https://hub.docker.com/repository/docker/fwelter/elastic-search-middleware).

If your Elasticsearch instance is not running locally at the standard port, specifiy your endpoint by
setting the environment variable `ES_HOST`.